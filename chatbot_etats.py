import logging, sys, requests, time, math

from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    ConversationHandler,
    CallbackContext,
)

bot_token = sys.argv[1]


# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)




#Definition State
STATE_CHOIX = "choice_between_restaurant_sortie"
STATE_RESTAURANTS_CHOIX = "restaurants_choix"
STATE_RESTAURANTS_RESULTAT = "restaurants_resultat"
STATE_RESTAURANTS_DETAILS = "restaurants_details"
STATE_SORTIE_CHOIX = "sortie_choix"
STATE_SORTIE_RECOMMENDATIONS = "sortie_recommendations"

#Definition State transport
STATE_TRANSPORT = "transport"

####################################PARTI RESTAURANT ET SORTIE####################################################################################

#Commande start choix entre restaurant et sortie

def start(update: Update, context: CallbackContext) -> int:
    reply_keyboard = [['Restaurant 🍽', 'Sortie 🎭']]
    user = update.message.from_user
    logger.info("User %s is starting the bot.", user.first_name)
    update.message.reply_text(
        f"Hey {update.effective_user.first_name}, je suis GenevaMipam et je vais t'aider à re profiter de Genève comme il se doit. 🧐\n\n"
        'Envoie /annuler si tu ne veux plus parler avec moi. 😭\n\n',
    )
    update.message.reply_text(
        'Que preféres-tu entre un restaurant 🍔 ou une sortie 🎭 ?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False,),
    )
    return STATE_CHOIX

#Choix du restaurant

def restaurants_choix(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("Choice of %s : %s ", user.first_name, update.message.text)
    reply_keyboard = [
        ['Americain 🍔', 'Italien 🍕'],
        ['Japonais 🍣', 'Indien 🍛', 'Chinois 🍜'],
        ['Retour']
                      ]

    update.message.reply_text(
        f'Excellent {update.effective_user.first_name}, tu as donc faim.\n'
        'Que souhaites-tu manger ?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False),
    )
    update.message.reply_video("https://media.giphy.com/media/12uXi1GXBibALC/giphy.gif")
    ReplyKeyboardRemove(remove_keyboard=True)
    return STATE_RESTAURANTS_CHOIX

#Choix de la sortie
def sortir_choix(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("Choice of %s : %s ", user.first_name, update.message.text)

    reply_keyboard = [
        ['Clubs 💃', 'Bars 🥂', 'Musées 🏛'],
        ['Retour']]

    update.message.reply_text(
        f"Parfait {update.effective_user.first_name}, tu veux donc t'amusez.\n"
        'Que souhaites-tu faire ?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False),
    )
    update.message.reply_video("https://media.giphy.com/media/g9582DNuQppxC/giphy.gif")
    ReplyKeyboardRemove(remove_keyboard=True)
    return STATE_SORTIE_CHOIX

#Top 3 des bars, musées et clubs

def clubs(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("Choice of %s : %s ", user.first_name, update.message.text)

    reply_keyboard = [['Revenir aux catégories']]
    update.message.reply_video("https://media.giphy.com/media/pHYaWbspekVsTKRFQT/giphy.gif")
    update.message.reply_text('Voici un Top 3 des clubs à Genève :',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False),)

    update.message.reply_text("*-TOP 1- Java Club*\n\n *Adresse*\n Fairmont Grand Hotel Geneva\n Quai du Mont-Blanc 19\n 1201 Geneve – Suisse\n\n *Horaires*\n Mercredi au Samedi\n 23:00 à 05:00\n www.javaclub.ch", parse_mode='Markdown',),
    update.message.reply_text("*-TOP 2- Mambo Club*\n\n *Adresse*\n Rue de Monthoux 60\n 1201 Geneve – Suisse\n\n *Horaires*\n Jeudi au Samedi\n 22:00 à 05:00\n www.mambo.ch", parse_mode='Markdown', ),
    update.message.reply_text("*-TOP 3- Baroque Club*\n\n *Adresse*\n Place de la Fusterie 12\n 1204 Genève – Suisse\n\n *Horaires*\n Jeudi au Samedi\n 00:00 à 05:00\n www.lebaroque.com", parse_mode='Markdown', ),

    return STATE_SORTIE_RECOMMENDATIONS

def musees(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("Choice of %s : %s ", user.first_name, update.message.text)
    reply_keyboard = [['Revenir aux catégories']]
    update.message.reply_video("https://media.giphy.com/media/ehJ4vNWRhinhMMvLxp/giphy.gif")
    update.message.reply_text('Voici un Top 3 des musées à Genève\n',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False), )


    update.message.reply_text("*-TOP 1- Muséum d'Histoire Naturelle*\n\n *Adresse*\n Route de Malagnou 1\n 1208 Geneve – Suisse\n\n *Horaires*\n Mardi au Dimanche\n 10:00 à 14:00\n institutions.ville-geneve.ch/fr/mhn/", parse_mode='Markdown', ),
    update.message.reply_text("*-TOP 2- MEG*\n\n *Adresse*\n Boulevard Carl-Vogt 65\n 1205 Geneve – Suisse\n\n *Horaires*\n Mardi au Dimanche \n 11:00 à 18:00\n www.ville-ge.ch/meg/", parse_mode='Markdown', ),
    update.message.reply_text("*-TOP 3- Musée Ariana*\n\n *Adresse*\n Rue de Lausanne 128\n 1202 Genève – Suisse\n\n *Horaires*\n Mardi au Dimanche\n 10:00 à 18:00\n institutions.ville-geneve.ch/fr/ariana/", parse_mode='Markdown', ),

    return STATE_SORTIE_RECOMMENDATIONS

def bars(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("Choice of %s : %s ", user.first_name, update.message.text)
    reply_keyboard = [['Revenir aux catégories']]

    update.message.reply_video("https://media.giphy.com/media/3osxYfo1Y0NtKdYJi0/giphy.gif")
    update.message.reply_text('Voici un Top 3 des bars à Genève\n',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False), )

    update.message.reply_text("*-TOP 1- Mr Barber*\n\n *Adresse*\n 5 rue Leschot\n 1205 Geneve – Suisse\n\n *Horaires*\n Lundi au samedi\n 17:00 à 02:00\n www.mrbarbergeneva.com", parse_mode='Markdown', ),
    update.message.reply_text("*-TOP 2- Le Scandale*\n\n *Adresse*\n 24 rue de Lausanne\n 1201 Geneve – Suisse\n\n *Horaires*\n Lundi au samedi \n 11:30 à 00:00\n www.lescandale.ch", parse_mode='Markdown', ),
    update.message.reply_text("*-TOP 3- Petit & Grand Bottle*\n\n *Adresse*\n 12 rue Henri Blanvalet\n 1207 Genève – Suisse\n\n *Horaires*\n Mardi au Samedi\n 17:00 à 02:00\n www.bottlebrothers.ch", parse_mode='Markdown', ),

    return STATE_SORTIE_RECOMMENDATIONS


#Top Cuisines avec leurs restaurants

def italien(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("Choice of %s : %s ", user.first_name, update.message.text)
    reply_keyboard = [
        ['La Cantinella', 'Pizza Lina', ],
        ['Revenir aux catégories']]

    update.message.reply_text("La pizza for ever !")
    update.message.reply_photo('https://www.rustica.fr/images/pizza-chorizo.jpg')
    update.message.reply_text(
        "Choisis maintenant ton restaurant avec parcimonie.",
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False),
    )

    return STATE_RESTAURANTS_RESULTAT


def americain(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("Choice of %s : %s ", user.first_name, update.message.text)
    reply_keyboard = [
        ['The Hamburger Foundation', 'Chic Chicken'],
        ['Revenir aux catégories']]

    update.message.reply_text("Fuck Yeah !")
    update.message.reply_photo(
        'https://lilleaddict.fr/wordpress/wp-content/uploads/2019/10/65149817_124241855478650_4097927829331474914_n-651x400.jpg')

    update.message.reply_text(
        "Choisis maintenant ton restaurant avec parcimonie.",
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False),
    )

    return STATE_RESTAURANTS_RESULTAT

def japonais(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("Choice of %s : %s ", user.first_name, update.message.text)
    reply_keyboard = [
        ['Misuji', 'Mikado'],
        ['Revenir aux catégories']]

    update.message.reply_text("Pourquoi pas...")
    update.message.reply_photo("https://cdn.unitycms.io/image/ocroped/400,400,1000,1000,0,0/H6DH3YL1dtk/6ytkvwOlaMDAbZIkaK1OZx.jpg")

    update.message.reply_text(
        "Choisis maintenant ton restaurant avec parcimonie.",
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False),
    )

    return STATE_RESTAURANTS_RESULTAT

def indien(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("Choice of %s : %s ", user.first_name, update.message.text)
    reply_keyboard = [
        ['Bombay', 'Punjabi'],
        ['Revenir aux catégories']]

    update.message.reply_text("Ouille c'est épicé ! Mais c'est bon..")
    update.message.reply_photo("https://cdn.smood.ch/restaurants/59156ca7bd28a8b42dafc449/item-royal-india-lausanne-restaurant-delivery.jpg?1495542237168")


    update.message.reply_text(
        "Choisis maintenant ton restaurant avec parcimonie.",
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False),
    )

    return STATE_RESTAURANTS_RESULTAT

def chinois(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("Choice of %s : %s ", user.first_name, update.message.text)
    reply_keyboard = [
        ['Baoti', 'Kung FU'],
        ['Revenir aux catégories']]

    update.message.reply_text("Tout est bon dans le cochon !")
    update.message.reply_photo("https://www.takeaway.com/be-fr/foodwiki/uploads/sites/3/2018/02/chine.jpg")
    update.message.reply_text(
        "Choisis maintenant ton restaurant avec parcimonie.",
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False),
    )

    return STATE_RESTAURANTS_RESULTAT

#Tout les restaurants
def misuji(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("Choice of %s : %s ", user.first_name, update.message.text)
    reply_keyboard = [['Restaurants Japonais']]

    update.message.reply_text(
        "*Bon appétit à Misuji*", parse_mode='Markdown',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False),)
    update.message.reply_html("<b>En raison de la situation actuelle, je te conseil de</b><a href='www.ubereats.com/ch/geneva/food-delivery/misuji/HFRidueVTQqW_FVsjEH-Vg'> Commander sur UberEats</a>")
    update.message.reply_html("<i>Utilise mon code pour recevoir 15CHF de réduction:</i>")
    update.message.reply_html("<b>eats-mipamgui</b>")
    update.message.reply_location(latitude=46.19529121858904, longitude=6.141372255658265)
    return STATE_RESTAURANTS_DETAILS

def mikado(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("Choice of %s : %s ", user.first_name, update.message.text)
    reply_keyboard = [['Restaurants Japonais']]

    update.message.reply_text(
        "*Bon appétit à Mikado*", parse_mode='Markdown',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False),)
    update.message.reply_html("<b>En raison de la situation actuelle, je te conseil de</b><a href='https://www.ubereats.com/ch/geneva/food-delivery/mikado-terrassiere/k-BwrP_cRpKS6GpJTxguvA'> Commander sur UberEats</a>")
    update.message.reply_html("<i>Utilise mon code pour recevoir 15CHF de réduction:</i>")
    update.message.reply_html("<b>eats-mipamgui</b>")
    update.message.reply_location(latitude=46.1987343596928, longitude=6.14265971192481)
    return STATE_RESTAURANTS_DETAILS

def bombay(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("Choice of %s : %s ", user.first_name, update.message.text)
    reply_keyboard = [['Restaurants Indien']]

    update.message.reply_text(
        "*Bon appétit à Bombay*", parse_mode='Markdown',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False),)
    update.message.reply_html("<b>En raison de la situation actuelle, je te conseil de</b><a href='https://www.ubereats.com/ch/geneva/food-delivery/bombay/_Z0X6_9RQPSgPDBLm1-btA'> Commander sur UberEats</a>")
    update.message.reply_html("<i>Utilise mon code pour recevoir 15CHF de réduction:</i>")
    update.message.reply_html("<b>eats-mipamgui</b>")
    update.message.reply_location(latitude=46.209846822521996, longitude=6.144715343587432)
    return STATE_RESTAURANTS_DETAILS

def punjabi(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("Choice of %s : %s ", user.first_name, update.message.text)
    reply_keyboard = [['Restaurants Indien']]

    update.message.reply_text(
        "*Bon appétit à Punjabi*", parse_mode='Markdown',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False),)
    update.message.reply_html("<b>En raison de la situation actuelle, je te conseil de</b><a href='https://www.ubereats.com/ch/geneva/food-delivery/punjabi/ZhMh9MOFRmmnVz_ogiFiaQ'> Commander sur UberEats</a>")
    update.message.reply_html("<i>Utilise mon code pour recevoir 15CHF de réduction:</i>")
    update.message.reply_html("<b>eats-mipamgui</b>")
    update.message.reply_location(latitude=46.200571281053016, longitude=6.131178064417723)
    return STATE_RESTAURANTS_DETAILS

def baoti(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("Choice of %s : %s ", user.first_name, update.message.text)
    reply_keyboard = [['Restaurants Chinois']]

    update.message.reply_text(
        "*Bon appétit à Baoti*", parse_mode='Markdown',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False),)
    update.message.reply_html("<b>En raison de la situation actuelle, je te conseil de</b><a href='https://www.ubereats.com/ch/geneva/food-delivery/baoti/CzAVYziHSOurjLOJrjTD8w'> Commander sur UberEats</a>")
    update.message.reply_html("<i>Utilise mon code pour recevoir 15CHF de réduction:</i>")
    update.message.reply_html("<b>eats-mipamgui</b>")
    update.message.reply_location(latitude=46.207526659900815, longitude=6.141603720240497)
    return STATE_RESTAURANTS_DETAILS

def kungfu(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("Choice of %s : %s ", user.first_name, update.message.text)
    reply_keyboard = [['Restaurants Chinois']]

    update.message.reply_text(
        "*Bon appétit à Kung Fu*", parse_mode='Markdown',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False),)
    update.message.reply_html("<b>En raison de la situation actuelle, je te conseil de</b><a href='https://www.ubereats.com/ch/geneva/food-delivery/kung-fu/yxTXuFy3Qx--fRQnq5gRQQ'> Commander sur UberEats</a>")
    update.message.reply_html("<i>Utilise mon code pour recevoir 15CHF de réduction:</i>")
    update.message.reply_html("<b>eats-mipamgui</b>")
    update.message.reply_location(latitude=46.208021633567064, longitude=6.1407190779113865)
    return STATE_RESTAURANTS_DETAILS

def thf(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("Choice of %s : %s ", user.first_name, update.message.text)
    reply_keyboard = [['Restaurants Américain']]

    update.message.reply_text(
        "*Bon appétit à The Hamburger Foundation*", parse_mode='Markdown',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False),)
    update.message.reply_html("<b>En raison de la situation actuelle, je te conseil de</b><a href='https://www.ubereats.com/ch/geneva/food-delivery/the-hamburger-foundation-plainpalais/0gCIHjsoTSOiZV2Edkey2Q'> Commander sur UberEats</a>")
    update.message.reply_html("<i>Utilise mon code pour recevoir 15CHF de réduction:</i>")
    update.message.reply_html("<b>eats-mipamgui</b>")
    update.message.reply_location(latitude=46.211644869083685, longitude=6.1508015067468325)
    return STATE_RESTAURANTS_DETAILS

def chicchicken(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("Choice of %s : %s ", user.first_name, update.message.text)
    reply_keyboard = [['Restaurants Américain']]

    update.message.reply_text(
        "*Bon appétit à Chic Chicken*", parse_mode='Markdown',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False),)
    update.message.reply_html("<b>En raison de la situation actuelle, je te conseil de </b><a href='https://www.ubereats.com/ch/geneva/food-delivery/the-hamburger-foundation-plainpalais/0gCIHjsoTSOiZV2Edkey2Q'> Commander sur UberEats</a>")
    update.message.reply_html("<i>Utilise mon code pour recevoir 15CHF de réduction:</i>")
    update.message.reply_html("<b>eats-mipamgui</b>")
    update.message.reply_location(latitude=46.19657354766092, longitude=6.142333275705886)
    return STATE_RESTAURANTS_DETAILS

def cantinella(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("Choice of %s : %s ", user.first_name, update.message.text)
    reply_keyboard = [['Restaurants Italien']]

    update.message.reply_text(
        "*Bon appétit à La Cantinella*", parse_mode='Markdown',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False),)
    update.message.reply_html("<b>En raison de la situation actuelle, je te conseil de</b><a href='https://www.ubereats.com/ch/geneva/food-delivery/la-cantinella/Ao0a0XcyS-qk7-HMosG8Bw'> Commander sur UberEats</a>")
    update.message.reply_html("<i>Utilise mon code pour recevoir 15CHF de réduction:</i>")
    update.message.reply_html("<b>eats-mipamgui</b>")
    update.message.reply_location(latitude=46.202760176564695, longitude=6.144753837430391)
    return STATE_RESTAURANTS_DETAILS

def pizzalina(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("Choice of %s : %s ", user.first_name, update.message.text)
    reply_keyboard = [['Restaurants Italien']]

    update.message.reply_text(
        "*Bon appétit à Pizza Lina*", parse_mode='Markdown',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False),)
    update.message.reply_html("<b>En raison de la situation actuelle, je te conseil de</b><a href='https://www.ubereats.com/ch/geneva/food-delivery/pizza-lina/7iGTXnYXReGl8fjAIt5RCg'> Commander sur UberEats</a>")
    update.message.reply_html("<i>Utilise mon code pour recevoir 15CHF de réduction:</i>")
    update.message.reply_html("<b>eats-mipamgui</b>")
    update.message.reply_location(latitude=46.21073807459583, longitude=6.145405993253165)
    return STATE_RESTAURANTS_DETAILS


def annuler(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text(
        'Au revoir ! Pour reparler avec moi fais /start', reply_markup=ReplyKeyboardRemove()
    )
    update.message.reply_video("https://giphy.com/gifs/the-simpsons-scared-homer-simpson-jUwpNzg9IcyrK")
    return conv_handler.END

###########################PARTI TRANSPORT############################################################################################


# Fonctions communes

def appeler_opendata(path):
    url = "https://transport.opendata.ch/v1/" + path
    result = requests.get(url)
    print(url)
    return result.json()

def afficher_arrets(arrets, update):
    for arret in arrets['stations']:
        if arret['id'] is not None:
            update.message.reply_text("/stop" + arret['id'] + " - " + arret['name'])


def calculer_temps(timestamp):
    seconds = timestamp - time.time()
    minutes = seconds/60;
    minutes = math.floor(minutes)

    if minutes < 0:
        return "Trop tard"

    if(minutes < 3):
        return "Presque parti"

    return "{} min".format(minutes)


# Récupération des actions à faire

def transport(update: Update, context: CallbackContext) -> None:
    update.message.reply_text(f"Bonjour {update.effective_user.first_name}, veuillez m'envoyer votre localisation (en texte ou en pièce jointe)")
    return STATE_TRANSPORT

def rechercher_par_localisation(update: Update, context: CallbackContext) -> None:
    location = update.message.location
    arrets = appeler_opendata("locations?x={}&y={}".format(location.longitude, location.latitude) )
    afficher_arrets(arrets, update)

def rechercher_par_nom(update: Update, context: CallbackContext) -> None:
    arrets = appeler_opendata("locations?query=" + update.message.text)
    afficher_arrets(arrets, update)


def afficher_horaires(update: Update, context: CallbackContext) -> None:
    arret_id = update.message.text[5:]
    prochains_departs = appeler_opendata("stationboard?id={}&limit=10".format(arret_id))

    reponse = "Voici les prochains départs:\n\n"

    for depart in prochains_departs["stationboard"]:
        stop = depart['stop']
        reponse = reponse + calculer_temps(stop['departureTimestamp']) + ' - Bus n°' + depart['number'] + " -> " + depart['to'] + "\n"

    reponse = reponse + "\nPour actualiser: /stop{}".format(arret_id)
    update.message.reply_text(reponse)


def cancel(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text(
        'Au revoir ! Pour reparler avec moi faites /transport', reply_markup=ReplyKeyboardRemove()
    )
    update.message.reply_video("https://giphy.com/gifs/the-simpsons-scared-homer-simpson-jUwpNzg9IcyrK")
    return new_handler.END


# Create the Updater and pass it your bot's token.
updater = Updater(bot_token)

# Get the dispatcher to register handlers
dispatcher = updater.dispatcher

# Conversation handler
conv_handler = ConversationHandler(
    entry_points=[CommandHandler('start', start)],
    states={
        STATE_CHOIX: [
            MessageHandler(Filters.regex('^(Restau|restau)'), restaurants_choix),
            MessageHandler(Filters.regex('^(Sortie|sortie)'), sortir_choix)],

        STATE_RESTAURANTS_CHOIX: [
            MessageHandler(Filters.regex('^(Americain)'), americain),
            MessageHandler(Filters.regex('^(Italien)'), italien),
            MessageHandler(Filters.regex('^(Japonais)'), japonais),
            MessageHandler(Filters.regex('^(Indien)'), indien),
            MessageHandler(Filters.regex('^(Chinois)'), chinois),
            MessageHandler(Filters.regex('^(Retour)'), start)],

        STATE_SORTIE_CHOIX: [
            MessageHandler(Filters.regex('^(Club)'), clubs),
            MessageHandler(Filters.regex('^(Bar)'), bars),
            MessageHandler(Filters.regex('^(Musée)'), musees),
            MessageHandler(Filters.regex('^(Musée)'), musees),
            MessageHandler(Filters.regex('^(Retour)'), start),
            ],

        STATE_SORTIE_RECOMMENDATIONS: [MessageHandler(Filters.regex('^(Revenir)'), sortir_choix)],

        STATE_RESTAURANTS_RESULTAT: [
            MessageHandler(Filters.regex('^(Misuji)'), misuji),
            MessageHandler(Filters.regex('^(Mikado)'), mikado),
            MessageHandler(Filters.regex('^(Bombay)'), bombay),
            MessageHandler(Filters.regex('^(Baoti)'), baoti),
            MessageHandler(Filters.regex('^(Kung)'), kungfu),
            MessageHandler(Filters.regex('^(The Hamburger)'), thf),
            MessageHandler(Filters.regex('^(Chic)'), chicchicken),
            MessageHandler(Filters.regex('^(La Cantinella)'), cantinella),
            MessageHandler(Filters.regex('^(Pizza)'), pizzalina),
            MessageHandler(Filters.regex('^(Punjabi)'), punjabi),
            MessageHandler(Filters.regex('^(Revenir)'), restaurants_choix)],

        STATE_RESTAURANTS_DETAILS: [
            MessageHandler(Filters.regex('^(Restaurants Japonais)$'), japonais),
            MessageHandler(Filters.regex('^(Restaurants Italien)$'), italien),
            MessageHandler(Filters.regex('^(Restaurants Chinois)$'), chinois),
            MessageHandler(Filters.regex('^(Restaurants Indien)$'), indien),
            MessageHandler(Filters.regex('^(Restaurants Américain)$'), americain),
        ],
    },
    fallbacks=[CommandHandler('annuler', annuler)],
)
dispatcher.add_handler(conv_handler)

#Nouveau conversation handler
new_handler = ConversationHandler(
    entry_points=[CommandHandler('transport', transport)],
    states={
        STATE_TRANSPORT: [
            MessageHandler(Filters.location, rechercher_par_localisation),
            MessageHandler(Filters.command, afficher_horaires),
            MessageHandler(Filters.text, rechercher_par_nom),
        ],

    },
    fallbacks=[CommandHandler('cancel', cancel)],
)

dispatcher.add_handler(new_handler)


# Start the Bot
updater.start_polling()


updater.idle()